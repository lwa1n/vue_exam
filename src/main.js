import { createApp } from 'vue'
import App from './App'
import router from '@/router/router'
import store from '@/store'
import vuetify from '@/plugins/vuetify'

import 'bootstrap/dist/css/bootstrap.css'
import bootstrap from 'bootstrap/dist/js/bootstrap.bundle.js'



const app = createApp(App);
app.use(router, bootstrap, store, vuetify)
app.mount('#app');