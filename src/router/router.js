import MainPage from "@/pages/MainPage.vue";
import ProjectPage from "@/pages/ProjectPage.vue";
import MainPageWithStore from "@/pages/MainPageWithStore.vue";
import { createRouter, createWebHashHistory } from "vue-router";

export default createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: MainPage
        },
        {
            path: '/:id/:title/:body',
            component: ProjectPage
        },
        {
            path: '/store',
            component: MainPageWithStore
        }
    ]
})